package br.com.mastertech.cartoes.models;

import br.com.mastertech.cartoes.models.dtos.CartaoDTO;
import br.com.mastertech.cartoes.models.dtos.CartaoResponseDTO;
import org.springframework.stereotype.Component;

@Component
public class CartaoMapper {

    public Cartao transformaCartao(CartaoDTO cartaoDTO, Cliente cliente){
        Cartao cartao = new Cartao(cartaoDTO.getNumero(), cliente, false);

        return cartao;
    }

    public CartaoResponseDTO transformaCartaoResposta(Cartao cartao, Cliente cliente){
        CartaoResponseDTO cartaoResponseDTO = new CartaoResponseDTO(cartao.getId(),cartao.getNumero(),cliente.getId(),
                cartao.getAtivo());

        return cartaoResponseDTO;
    }


}
