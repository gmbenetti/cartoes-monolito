package br.com.mastertech.cartoes.models.dtos;

public class CartaoStatusDTO {

    private Boolean ativo;

    public CartaoStatusDTO() {
    }

    public CartaoStatusDTO(Boolean ativo) {
        this.ativo = ativo;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }
}
