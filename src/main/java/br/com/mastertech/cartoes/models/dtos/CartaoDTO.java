package br.com.mastertech.cartoes.models.dtos;

public class CartaoDTO {

    private String numero;
    private Long clienteId;

    public CartaoDTO() {
    }

    public CartaoDTO(String numero, Long clienteId) {
        this.numero = numero;
        this.clienteId = clienteId;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Long getClienteId() {
        return clienteId;
    }

    public void setClienteId(Long clienteId) {
        this.clienteId = clienteId;
    }
}
