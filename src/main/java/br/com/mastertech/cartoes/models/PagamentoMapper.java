package br.com.mastertech.cartoes.models;

import br.com.mastertech.cartoes.models.dtos.PagamentoDTO;
import org.springframework.stereotype.Component;

@Component
public class PagamentoMapper {

    public Pagamento transformaPagamento(PagamentoDTO pagamentoDTO, Cartao cartao) {
        Pagamento pagamento = new Pagamento(pagamentoDTO.getDescricao(),
                pagamentoDTO.getValor(), cartao);

        return pagamento;
    }

    public PagamentoDTO transformaPagamentoDTO(Pagamento pagamento){
        PagamentoDTO pagamentoDTO = new PagamentoDTO(pagamento.getCartao().getId(),
                pagamento.getDescricao(), pagamento.getValor());

        return  pagamentoDTO;
    }
}
