package br.com.mastertech.cartoes.services;

import br.com.mastertech.cartoes.models.Cartao;
import br.com.mastertech.cartoes.models.Pagamento;
import br.com.mastertech.cartoes.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PagamentoService {
    @Autowired
    PagamentoRepository pagamentoRepository;

    public Pagamento salvar(Pagamento pagamento){
        Pagamento pagamentoObjeto = pagamentoRepository.save(pagamento);
        return pagamentoObjeto;
    }

    public List<Pagamento> buscarPorCartao(Cartao cartao) {
        List<Pagamento> pagamentos = pagamentoRepository.findAllByCartao(cartao);
        if(pagamentos.size() > 0){
            return pagamentos;
        }else{
            throw new RuntimeException("Cartão id " + cartao.getId() + " não possui pagamentos");
        }
    }
}
