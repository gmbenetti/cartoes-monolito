package br.com.mastertech.cartoes.services;

import br.com.mastertech.cartoes.models.Cartao;
import br.com.mastertech.cartoes.models.CartaoMapper;
import br.com.mastertech.cartoes.models.Cliente;
import br.com.mastertech.cartoes.models.dtos.CartaoDTO;
import br.com.mastertech.cartoes.models.dtos.CartaoResponseDTO;
import br.com.mastertech.cartoes.repositories.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {
    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private CartaoMapper cartaoMapper;

    public Cartao salvar(Cartao cartao){
        Cartao cartaoObjeto = cartaoRepository.save(cartao);

        return  cartaoObjeto;
    }

    public Cartao buscarPorNumero(String numeroCartao){
        Optional<Cartao> cartaoOptional = cartaoRepository.findByNumero(numeroCartao);

        if(cartaoOptional.isPresent()){
            Cartao cartao = cartaoOptional.get();
            return cartao;
        }
        throw new RuntimeException("Cartão inexistente");
    }

    public Cartao alteraStatus(Cartao cartao, Boolean ativo){
        cartao.setAtivo(ativo);
        Cartao cartaoObjeto = salvar(cartao);
        return cartaoObjeto;
    }

    public Cartao buscarPorId(Long cartao_id) {
        Optional<Cartao> cartaoOptional = cartaoRepository.findById(cartao_id);

        if(cartaoOptional.isPresent()){
            Cartao cartao = cartaoOptional.get();
            return cartao;
        }
        throw  new RuntimeException("Cartão inexistente");
    }
}
