package br.com.mastertech.cartoes.services;

import br.com.mastertech.cartoes.models.Cliente;
import br.com.mastertech.cartoes.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente salvar(Cliente cliente){
        Cliente clienteObjeto = clienteRepository.save(cliente);

        return clienteObjeto;
    }

    public List<Cliente> buscarTodos(){
        List<Cliente> clientes = (List) clienteRepository.findAll();
        return clientes;
    }

    public Cliente buscarPorId(Long id){
        Optional<Cliente> clienteOptional = clienteRepository.findById(id);

        if(clienteOptional.isPresent()){
            Cliente cliente = clienteOptional.get();
            return cliente;
        }
        throw new RuntimeException("Cliente id " + id + " informado, não encontrado");
    }
}
