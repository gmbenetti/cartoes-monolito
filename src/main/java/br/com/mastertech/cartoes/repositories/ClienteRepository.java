package br.com.mastertech.cartoes.repositories;

import br.com.mastertech.cartoes.models.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente, Long> {



}
