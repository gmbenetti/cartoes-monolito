package br.com.mastertech.cartoes.repositories;

import br.com.mastertech.cartoes.models.Cartao;
import br.com.mastertech.cartoes.models.Pagamento;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PagamentoRepository extends CrudRepository<Pagamento, Long> {
    List<Pagamento> findAllByCartao(Cartao cartao);
}