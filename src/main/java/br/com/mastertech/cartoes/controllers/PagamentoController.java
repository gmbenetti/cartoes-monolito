package br.com.mastertech.cartoes.controllers;

import br.com.mastertech.cartoes.models.Cartao;
import br.com.mastertech.cartoes.models.Pagamento;
import br.com.mastertech.cartoes.models.PagamentoMapper;
import br.com.mastertech.cartoes.models.dtos.PagamentoDTO;
import br.com.mastertech.cartoes.services.CartaoService;
import br.com.mastertech.cartoes.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping
public class PagamentoController {

    @Autowired
    PagamentoService pagamentoService;

    @Autowired
    CartaoService cartaoService;

    @Autowired
    PagamentoMapper pagamentoMapper;

    @PostMapping("/pagamento")
    public ResponseEntity<PagamentoDTO> gravarPagamento(@RequestBody @Valid PagamentoDTO pagamentoDTO){

        Cartao cartao = cartaoService.buscarPorId(pagamentoDTO.getCartaoId());
        Pagamento pagamento = pagamentoMapper.transformaPagamento(pagamentoDTO, cartao);
        Pagamento pagamentoObjeto = pagamentoService.salvar(pagamento);
        PagamentoDTO pagamentoDTOObjeto = pagamentoMapper.transformaPagamentoDTO(pagamentoObjeto);
        return ResponseEntity.status(201).body(pagamentoDTO);
    }

    @GetMapping("/pagamentos/{idCartao}")
    public ResponseEntity<List<PagamentoDTO>> consulta(@PathVariable(name="idCartao")Long idCartao){
        try{
            Cartao cartao = cartaoService.buscarPorId(idCartao);
            List<Pagamento> pagamentos = pagamentoService.buscarPorCartao(cartao);
            List<PagamentoDTO> pagamentosDTO = new ArrayList<>();

            for (Pagamento pagamento: pagamentos) {
                PagamentoDTO pagamentoDTO = pagamentoMapper.transformaPagamentoDTO(pagamento);
                pagamentosDTO.add(pagamentoDTO);
            }

            return ResponseEntity.status(200).body(pagamentosDTO);

        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }


}
