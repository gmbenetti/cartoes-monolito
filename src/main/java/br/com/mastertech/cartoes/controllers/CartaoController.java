package br.com.mastertech.cartoes.controllers;

import br.com.mastertech.cartoes.models.Cartao;
import br.com.mastertech.cartoes.models.CartaoMapper;
import br.com.mastertech.cartoes.models.Cliente;
import br.com.mastertech.cartoes.models.dtos.CartaoDTO;
import br.com.mastertech.cartoes.models.dtos.CartaoResponseDTO;
import br.com.mastertech.cartoes.models.dtos.CartaoStatusDTO;
import br.com.mastertech.cartoes.services.CartaoService;
import br.com.mastertech.cartoes.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private CartaoMapper cartaoMapper;

    @PostMapping
    public ResponseEntity<CartaoResponseDTO> registrarCartao(@RequestBody @Valid CartaoDTO cartaoDTO){
        Optional<Cliente> clienteOptional = Optional.ofNullable(clienteService.buscarPorId(cartaoDTO.getClienteId()));

        if(clienteOptional.isPresent()) {
            Cliente cliente = clienteOptional.get();
            Cartao cartao = cartaoMapper.transformaCartao(cartaoDTO, cliente);
            Cartao cartaoObjeto = cartaoService.salvar(cartao);
            CartaoResponseDTO cartaoResponseDTO = cartaoMapper.transformaCartaoResposta(cartaoObjeto,cliente);
            return ResponseEntity.status(201).body(cartaoResponseDTO);
        }
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
    }

    @PatchMapping("/{numero}")
    public ResponseEntity<CartaoResponseDTO> alteraStatus(@RequestBody @Valid CartaoStatusDTO cartaoStatusDTO,
                                                          @PathVariable(name="numero")String numeroCartao){
        Cartao cartao  = cartaoService.buscarPorNumero(numeroCartao);

        Cartao cartaoObjeto = cartaoService.alteraStatus(cartao, cartaoStatusDTO.getAtivo());

        Cliente cliente = cartaoObjeto.getCliente();
        CartaoResponseDTO cartaoResponseDTO = cartaoMapper.transformaCartaoResposta(cartaoObjeto, cliente);

        return  ResponseEntity.status(200).body(cartaoResponseDTO);
    }

    @GetMapping("/{numero}")
    public  ResponseEntity<CartaoResponseDTO> consultar(@PathVariable(name="numero")String numero) {
        try {
            Cartao cartao = cartaoService.buscarPorNumero(numero);
            CartaoResponseDTO cartaoResponseDTO = cartaoMapper.transformaCartaoResposta(cartao, cartao.getCliente());
            return ResponseEntity.status(200).body(cartaoResponseDTO);
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }
}
